import { success, failure } from '../../../libs/response-lib';
import * as dynamoDbLib from '../../../libs/dynamodb-lib';

export async function main (event, context, callback) {
  const data = event.mock ? event.body : JSON.parse(event.body);
  let params = {
    TableName: process.env.dogsTableName,
    Key: {
      id: event.pathParameters.dogId
    },
    UpdateExpression: 'set #name = :name, #breed = :breed',
    ExpressionAttributeNames: {
      '#name': 'name',
      '#breed': 'breed'
    },
    ExpressionAttributeValues: {
      ':name': data.name,
      ':breed': data.breed
    },
    ReturnValues: 'ALL_NEW'
  };
  try {
    let result = await dynamoDbLib.call('update', params);
    return success({ result: result.Attributes });
  } catch (e) {
    return failure({
      error: {
        message: e,
        fields: '',
        stack: e.stack
      }
    });
  }
}
