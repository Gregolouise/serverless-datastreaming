import * as dynamoDbLib from '../../../libs/dynamodb-lib';
import { success, failure } from '../../../libs/response-lib';

export async function main (event, context) {
  try {
    const Dogs = await dynamoDbLib.call('scan', {
      TableName: process.env.dogsTableName
    });
    return success({ result: Dogs.Items });
  } catch (e) {
    return failure({ error: e });
  }
}
