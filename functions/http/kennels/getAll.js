import * as dynamoDbLib from '../../../libs/dynamodb-lib';
import { success, failure } from '../../../libs/response-lib';

export async function main (event, context) {
  try {
    const Kennels = await dynamoDbLib.call('scan', {
      TableName: process.env.kennelsTableName
    });
    return success({ result: Kennels.Items });
  } catch (e) {
    return failure({ error: e });
  }
}
