import { success, failure } from '../../../libs/response-lib';
import * as dynamoDbLib from '../../../libs/dynamodb-lib';

export async function main (event, context, callback) {
  const data = event.mock ? event.body : JSON.parse(event.body);
  let params = {
    TableName: process.env.kennelsTableName,
    Key: {
      id: event.pathParameters.kennelId
    },
    UpdateExpression: 'set #name = :name',
    ExpressionAttributeNames: {
      '#name': 'name'
    },
    ExpressionAttributeValues: {
      ':name': data.name
    },
    ReturnValues: 'ALL_NEW'
  };
  try {
    let result = await dynamoDbLib.call('update', params);
    return success({ result: result.Attributes });
  } catch (e) {
    return failure({
      error: {
        message: e,
        fields: '',
        stack: e.stack
      }
    });
  }
}
