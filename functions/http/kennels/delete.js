import * as dynamoDbLib from '../../../libs/dynamodb-lib';
import { success, failure } from '../../../libs/response-lib';

export async function main (event, context) {
  const params = {
    TableName: process.env.kennelsTableName,
    Key: {
      id: event.pathParameters.kennelId
    }
  };

  try {
    await dynamoDbLib.call('delete', params);
    return success();
  } catch (e) {
    return failure({ error: e });
  }
}
